@extends('app')

@section('content')
    <table class="table message-list">
        <tr>
            <th>Title</th>
            <th>Date</th>
            @if (App\User::isAdmin())
                <th>Admin Functions</th>
            @endif
        </tr>
    @foreach($posts as $post)
            <tr>
                <td>
                    <a href="/articles/{{rawurlencode(strtolower(str_replace(' ', '-',$post->title)))}}">{{$post->title}}</a>
                </td>
                <td>
                    {{ $post->created_at }}
                </td>
                @if (App\User::isAdmin())
                <td>
                    {!! Form::open(array('url' => '/articles/'.$post->id, 'method' => 'delete')) !!}
                    {!! Form::submit('Delete', array('class' => 'btn btn-default confirm-delete')) !!}
                    <a class="btn btn-default" href="/articles/{{$post->id}}/edit">Edit</a>
                </td>
                @endif
            </tr>

    @endforeach
    </table>
    {!! $posts->render() !!}

@endsection
