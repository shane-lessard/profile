@extends('app')

@section('content')

                        <table class="table">
                            <tr>

                                <th>Subject</th>
                                <th>Contact</th>
                                <th>Date</th>
                                <th>Admin</th>

                            </tr>
                            @foreach($messages as $message)
                                <tr>
                                    <td>
                                        <a href="/contact/message/{{$message->id}}">{{$message->subject}}</a>
                                    </td>
                                    <td>
                                        {{$message->contact}}
                                    </td>
                                    <td>
                                        {{ $message->created_at }}
                                    </td>
                                    <td>
                                        {!! Form::open(array('url' => '/contact/remove-message/'.$message->id)) !!}
                                        {!! Form::submit('Delete', array('class' => 'btn btn-default')) !!}
                                    </td>
                                </tr>

                            @endforeach
                        </table>
                        {!! $messages->render() !!}

@endsection
