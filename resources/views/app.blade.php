<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Shane Lessard{{ isset($title) ? " - " . $title : "" }}</title>

    <link rel="icon" href="/assets/favicon.ico" type="image/x-icon" />

    <!-- Styles -->

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/prism/0.0.1/prism.min.css">

    <link href="{{ asset('/css/app.css') }}" rel="stylesheet">

    <!-- Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Lato:400,100italic,100,300,300italic,400italic,700,700italic,900,900italic|Roboto:400,700,200' rel='stylesheet' type='text/css'><!--google font-->

</head>
<body>
	<nav class="navbar navbar-default">
		<div class="container">
            <div class="col-md-10 col-md-offset-1">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/">Shane Lessard</a>
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">


                    <ul class="nav navbar-nav navbar-right">

                        @if(count(App\Post::all()) > 0)
                            <li><a href="/articles">Latest Articles</a></li>
                        @endif

                        <li><a href="/contact">Contact Me</a></li>
                        <li><a href="/portfolio">Portfolio</a></li>

                        @if(Auth::check() && Auth::user()->role != 'admin')
                            <li><a href="/auth/logout">Logout</a></li>
                        @endif

                        @if(App\User::isAdmin())

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Admin <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="/articles/create">New Article</a></li>
                                <li><a href="/portfolio/create">New Portfolio Item</a></li>
                                <li><a href="/contact/message-list">Messages</a></li>
                                <li><a href="/auth/logout">Logout</a></li>

                            </ul>
                        </li>

                        @endif

                    </ul>
                </div>
            </div>
		</div>
	</nav>
    <section class="body">
        <div class="container">
            <div class="row">
                <div class="{{ isset($bodyWidth) ? $bodyWidth : 'col-md-10 col-md-offset-1'}}">
                    <div class="panel panel-default">
                        <div class="panel-heading {{isset($titleClass)? $titleClass : ""}}">
                            <h2>{{isset($title) ? $title : ""}}
                            @if (isset($icons) && $icons == true)
                                <div class="contact-icons">
                                    <a href="https://github.com/shane-lessard"><i class="fa fa-github"></i></a>
                                    <a href="https://twitter.com/shane_lessard"><i class="fa fa-twitter"></i></a>
                                    <a href="https://ca.linkedin.com/in/lessardshane"><i class="fa fa-linkedin"></i></a>
                                </div>
                            @endif
                            </h2>
                        </div>

                        <article class="panel-body">
	                        @yield('content')
                        </article>
                    </div>
                </div>
            </div>
        </div>
    </section>
	<!-- Scripts -->
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/prism/0.0.1/prism.min.js"></script>
    <script src="/scripts/app.js"></script>

    @if(getenv('APP_ENV') == "production")
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-52493601-5', 'auto');
            ga('send', 'pageview');

        </script>
    @endif

</body>
</html>
