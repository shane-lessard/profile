@extends('app')

@section('content')

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <p><strong>It looks like something went wrong!</strong></p>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <p>The easiest way to get ahold of me is to email: <a href="mailto:dev@shanelessard.com">dev@shanelessard.com</a>, or to call 226-350-4242. Alternatively, use the form below.</p>

    <p>If you need a quick response and are contacting me about a potential project, contract, collaboration or opportunity, be sure to leave as much detail as possible.</p>

    <div class="form">
            {!! Form::open(array('url' => '/contact')) !!}

        <div class="form-group">

            {!! Form::label('Return Address (or phone number)') !!}

            {!! Form::text('contact', null, array(
            'class' => 'form-control',
            )) !!}

        </div>

        <div class="form-group">

            {!! Form::label('Subject Line') !!}

            {!! Form::text('subject', null, array(
            'class' => 'form-control',
            )) !!}

        </div>
        <div class="form-group">

            {!! Form::label('message') !!}

            {!! Form::textarea('body', null, array(
            'class' => 'form-control',
            'rows' => '15',
            )) !!}

        </div>
        <div class="space-bottom captcha">
            {!! app('captcha')->display() !!}

        </div>
        <div class="form-group">


            {!! Form::submit('Send', array('class' => 'btn btn-default form-control')) !!}

        </div>
        <div class="form-group">

            {!! Form::close() !!}

        </div>

    </div>

@endsection