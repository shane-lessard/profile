<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Support\Facades\Auth;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'email', 'password'];

    /**
     * Check for admin role
     *
     * @return bool
     */
    static function isAdmin()
    {
        return Auth::check() && Auth::user()->role == 'admin' ? true : false;

    }

    /**
     * Check fo associates role
     *
     * @return bool
     */
    static function isAssoc()
    {
        return Auth::check() && Auth::user()->role == 'associate' ? true : false;
    }

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

    public function posts()
    {
        $this->hasMany('App\Post');
    }

}
