@extends('app')

@section('content')

    {!! $message->body !!}

    <br />
    <br />
    <strong>Reply to:</strong> <br />
    {{ $message->contact }}

@endsection
