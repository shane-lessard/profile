@extends('app')

@section('content')

    <div class="form">

        {!! Form::open(array(
        'url' => '/articles/'.$post->id.'',
        'method' => 'PUT'
        )) !!}

        <div class="form-group">

            {!! Form::label('title') !!}

            {!! Form::text('title', $post->title, array(
            'class' => 'form-control',
            )) !!}

        </div>
        <div class="form-group">

            {!! Form::label('body') !!}

            {!! Form::textarea('body', $post->body, array(
            'class' => 'form-control',
            'rows' => '30',
            )) !!}

        </div>

        {!! Form::submit('Update', array('class' => 'btn btn-default form-control')) !!}

        {!! Form::close() !!}

    </div>

@endsection
