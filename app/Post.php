<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model {

	public $fillable = [
    'title',
    'body',
    'user_id'
    ];

    public function user()
    {
        $this->ownedBy('App\User');
    }

}
