$(document).ready(function(){

    /**
     * add event handler to delete buttons to confirm deletion
     */
    $('.confirm-delete').on('click', function(){

        return confirm("Are you sure you want to remove that?");

    });

});
