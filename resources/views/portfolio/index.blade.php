@extends('app')

@section('content')

    @foreach($portfolios as $portfolio)

        <div class="col-xs-12 portfolio-row">

            <div class="col-xs-12 col-md-6 portfolio-image">
                <a target="_blank" href="{{ $portfolio->url }}"><img src="{{ $portfolio->image->url('thumb') }}" alt="{{ $portfolio->title }}" /></a>
            </div>

            <div class="col-xs-12 col-md-6 portfolio-description">
                <h2>{{ $portfolio->title }}</h2>
                <p>{!! nl2br($portfolio->description) !!}</p>
                <p><strong>{{ $portfolio->references }}</strong></p>
                <p><a target="_blank" href="{{ $portfolio->url }}">Check it out</a></p>
                @if($admin)
                    {!! Form::open(array('url' => '/portfolio/'.$portfolio->id, 'method' => 'delete')) !!}
                    {!! Form::submit('Delete', array('class' => 'btn btn-default confirm-delete')) !!}
                    <a class="btn btn-default" href="/portfolio/{{$portfolio->id}}/edit">Edit</a>
                @endif
            </div>

        </div>

    @endforeach

@endsection