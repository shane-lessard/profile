@extends('app')

@section('content')

    <div class="intro-text col-xs-12">

        <p>I'm an intermediate - full stack - web developer, specializing in the LAMP stack and based in Windsor, Ontario. I'm always interested in leaving my comfort zone to work with new and shiny technologies.</p>

    </div>

    <div class="col-xs-12">

        <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-0">

            <h3 class="space-bottom">Skillset</h3>

            <h4>Most Comfortable Skills:</h4>
            <ul>
                <li>PHP</li>
                <li>Laravel</li>
                <li>SQL</li>
                <li>Javascript</li>
                <li>jQuery</li>
                <li>HTML5/CSS</li>
            </ul>

            <h4>Hobby Level Skills:</h4>
            <ul>
                <li>C#</li>
                <li>ASP.NET MVC5</li>
                <li>NodeJS</li>
                <li>Vue.JS</li>
            </ul>

        </div>
        <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-0">

            <div>

                <h3 class="space-bottom">Availability</h3>

                <p>I'm available! Hit me up for any of these reasons:</p>

                <ul>
                    <li>Interesting web projects</li>
                    <li>Interesting not-web projects</li>
                    <li>Volunteer oportunities</li>
                    <li>Networking</li>
                </ul>

            </div>

            <div>
                @if(count($posts) > 0)
                    <h3 class="space-bottom">Latest Articles</h3>

                    @foreach($posts as $post)
                        <p><a href="/articles/{{rawurlencode(strtolower(str_replace(' ', '-',$post->title)))}}">{{$post->title}}</a></p>
                    @endforeach
                @endif

            </div>
        </div>
    </div>

@endsection
