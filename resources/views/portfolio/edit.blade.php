@extends('app')

@section('content')

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <p><strong>It looks like something went wrong!</strong></p>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="form">

        {!! Form::open(['url' => '/portfolio/'.$portfolio->id, 'method' => 'PUT', 'files' => true]) !!}

        <div class="form-group">

            {!! Form::label('Title') !!}
            {!! Form::text('title', $portfolio->title, ['class' => 'form-control']) !!}

        </div>
        <div class="form-group">

            {!! Form::label('Description') !!}
            {!! Form::textArea('description', $portfolio->description, ['class' => 'form-control']) !!}

        </div>
        <div class="form-group">

            {!! Form::label('references') !!}
            {!! Form::text('references', $portfolio->references, ['class' => 'form-control']) !!}

        </div>

        <div class="form-group">

            {!! Form::label('url') !!}
            {!! Form::text('url', $portfolio->url, ['class' => 'form-control']) !!}

        </div>

        <div class="form-group">

            {!! Form::label('Image') !!}
            {!! Form::file('image', null, ['class' => 'form-control form-inline']) !!}

        </div>

        <div class="form-group">

            {!! Form::label('Weight') !!}
            {!! Form::input('number', 'weight', $portfolio->weight, ['class' => 'form-control']) !!}

        </div>

        <div class="form-group">

            {!! Form::submit('save', ['class' => 'form-control']) !!}

        </div>

        {!! Form::close() !!}

    </div>


@endsection
