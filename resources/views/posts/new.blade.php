@extends('app')

@section('content')

    <p>Blog Posts use Markdown. <a target="_blank" href="https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet">Click here</a> for a Cheat sheet</p>
    <div class="form">

        {!! Form::open(array('url' => '/articles')) !!}

    <div class="form-group">

        {!! Form::label('title') !!}

        {!! Form::text('title', null, array(
            'class' => 'form-control',
            )) !!}

    </div>
    <div class="form-group">

    {!! Form::label('body') !!}

    {!! Form::textarea('body', null, array(
        'class' => 'form-control',
        'rows' => '30',
        )) !!}

    </div>

    {!! Form::submit('Save', array('class' => 'btn btn-default form-control')) !!}

    {!! Form::close() !!}


    </div>

@endsection
