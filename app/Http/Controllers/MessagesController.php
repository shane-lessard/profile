<?php
namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Input;
use Illuminate\Http\Request;
use App\Message;
use Validator;
use Redirect;
use Parsedown;
use DB;

class MessagesController extends Controller {

    public function __construct()
    {
        $this->middleware('admin', [
            'only' => [
                'getMessageList',
                'getMessage',
                'postRemoveMessage',
                'getRemoveMessages',
            ]
        ]);
    }

    public function getIndex()
    {
        return view('pages.contact', [
            'title' => "Contact Me",
            'icons' => true,
        ]);
    }

    public function postIndex(Request $request)
    {
        //mail form

        $input = $request->all();

        $validation = Validator::make($input, Message::$rules, Message::$messages);

        if ($validation->fails()) {

            $request->flash();
            return Redirect::back()
                ->withInput()
                ->withErrors($validation->messages());
        }

        $parse = new Parsedown;

        $message = $parse->text($input['body']) .'<p>Contact: '.$input['contact'].'</p><p>More at: http://shanelessard.com/contact/message-list</p>';

        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $headers .= 'From: Shane <admin@shanelessard.com>';

        /*
         * Notify me via email
         */
        mail('Shane <dev@shanelessard.com>',
           'New message: ' . $input['subject'],
            $message,
            $headers
        );

        //save form

        $message = Message::create($input);

        $message->save();

        return redirect('contact/thank-you');
    }

    public function getThankYou()
    {
        return view('pages.thank-you', ['title' => 'Thank you!']);
    }

    public function getMessageList()
    {

        $data = [
            'title' => "Recent Messages",
            'messages' => Message::orderBy('created_at', 'desc')->paginate(20),
        ];

        return view('pages.recent-messages', $data);

    }

    public function getMessage($id)
    {
        $parsedown = new Parsedown;
        $message = Message::find($id);

        $message->body = $parsedown->text($message->body);

        $data = [
            'title' => $message->subject,
            'message' => $message,
        ];

        return view('pages.message', $data);

    }

    public function postRemoveMessage($id)
    {
        Message::find($id)->delete();

        return redirect('/contact/message-list');
    }

    public function getRemoveMessages()
    {
        DB::table('messages')->delete();

        return redirect('/contact/message-list');
    }

}
