@extends('app')

@section('content')

    {!! $post->body !!}
    <div>
    <p class="text-right">Posted: <em>{{$post->created_at}}</em></p>
    </div>
    <hr />
        <div id="disqus_thread"></div>
        <script>


             var disqus_config = function () {
             this.page.url = window.location;
             this.page.identifier = window.location;
             };

            (function() { // DON'T EDIT BELOW THIS LINE
                var d = document, s = d.createElement('script');

                s.src = '//shanelessard.disqus.com/embed.js';

                s.setAttribute('data-timestamp', +new Date());
                (d.head || d.body).appendChild(s);
            })();
        </script>
        <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>

    @if (Auth::user() && Auth::user()->id == 1)

        {!! Form::open(array('url' => '/articles/'.$post->id, 'method' => 'delete', 'class' => 'form-right')) !!}
        {!! Form::submit('Delete', array('class' => 'btn btn-default confirm-delete')) !!}

        <a class="pull-right btn btn-default space-left" href="/articles/{{$post->id}}/edit">Edit</a>

    @endif

@endsection