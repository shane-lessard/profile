<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait;

class Portfolio extends Model implements StaplerableInterface {

    //neccessary for Stapler Interface
    use EloquentTrait;

    /**
     * Portfolio constructor.
     * @param array $attributes to pass to parent constructor
     */
    public function __construct(array $attributes = array()) {

        //Set styles for image attachments
        $this->hasAttachedFile('image', [
            'styles' => [
                'thumb' => '400x225',
                'large' => '800x450'
            ]
        ]);

        parent::__construct($attributes);
    }

    /**
     * @var array of fillable fields
     */
    protected $fillable = [
        'title',
        'references',
        'description',
        'image',
        'url',
        'weight',
    ];

    /**
     * @var array of guarded fields
     */
    protected $guarded = [
        'id',
    ];


}
