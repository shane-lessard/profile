<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model {

	public $fillable = array(
        'subject',
        'body',
        'contact',
    );

    static $rules = array(
        'subject' => 'required|max:40',
        'body' => 'required|max:2500',
        'contact' => 'required|max:50',
        'g-recaptcha-response' => 'required|captcha',

    );

    static $messages = array(

            'subject.required' => "Please add a subject line!",
            'subject.max' => "Please keep that subject line under 40 characters",
            'body.required' => 'Please include a message in your message',
            'body.max' => 'Please keep that message under 2500 characters',
            'contact.required' => "Please include some contact information so I can respond",
            'contact.max' => "Please keep your contact information under 50 characters",
            'g-recaptcha-response.required' => "Please confirm that you're not some kind of robot",

    );

}
