<?php namespace App\Http\Controllers;

use Input;
use App\Message;
use App\Post;

class HomeController extends Controller {


	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function getIndex()
	{

        $data = [
            'title' => 'Web Developer | Digital Consultant',
            'posts' => Post::orderby('created_at', 'desc')->take(5)->get(),
            'titleClass' => 'text-center',
        ];

		return view('home', $data);
	}

    /**
     * redirects
     */

    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function getHome()
    {
        return redirect('/');
    }

    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function getLogin()
    {
        return redirect('/auth/login');
    }

    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function getLogout()
    {
        return redirect('/auth/logout');
    }

    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function getRegister()
    {
        return redirect('/auth/logout');
    }

}
