-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Jan 12, 2016 at 08:59 AM
-- Server version: 5.5.42
-- PHP Version: 5.6.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `profile`
--

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `subject` text COLLATE utf8_unicode_ci NOT NULL,
  `body` longtext COLLATE utf8_unicode_ci NOT NULL,
  `contact` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `created_at`, `updated_at`, `subject`, `body`, `contact`) VALUES
(1, '2016-01-12 06:08:47', '2016-01-12 06:08:47', 'sadfvxzcagsd', 'vcxzbasdzvxc zv', 'asfdcvxz');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_01_05_231750_create_posts_table', 1),
('2016_01_08_015640_create_messages_table', 1),
('2016_01_08_234313_add_role_to_user', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `title` varchar(35) COLLATE utf8_unicode_ci NOT NULL,
  `body` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `user_id`, `title`, `body`, `created_at`, `updated_at`) VALUES
(2, 1, 'How To: Chain Methods in PHP', 'Chaining methods is simple to do in PHP, yet often  under-explained. If you aren’t sure what chaining methods is, it''s the ability to call multiple methods on a _class instance_ in one line. \r\nFor example - you may want to save a user, sign them in and then redirect them to a user profile. \r\n\r\nChaining each method together is a pretty way to express that flow of actions in your controller, like so:\r\n\r\n```php\r\n$user->save()->authenticate()->getUserProfile();\r\n```\r\n\r\nGenerally, the only thing you have to do to make this possible is to return ```$this``` in your methods. \r\n\r\nFor example, if I''m writing a class that defines a string with the _constructor_ and has methods to alter that string. I''ll use a method to convert the string to uppercase, a method to replace spaces with hyphens, and then finally a method to return the string. See below:\r\n\r\n```php\r\n<?php\r\n\r\nclass Foo\r\n{\r\n\r\n    /**\r\n     * @var string\r\n     */\r\n    protected $string;\r\n\r\n    /**\r\n     * @param $string\r\n     */\r\n    public function __construct($string)\r\n    {\r\n        $this->string = $string;\r\n    }\r\n\r\n    /**\r\n     * Change string to uppercase\r\n     *\r\n     * @return $this\r\n     */\r\n    public function toUpper()\r\n    {\r\n\r\n        $this->string = strtoupper($this->string);\r\n\r\n        return $this;\r\n    }\r\n\r\n    /**\r\n     * Replace spaces with hyphens\r\n     *\r\n     * @return $this\r\n     */\r\n    public function replaceSpaces()\r\n    {\r\n\r\n        $this->string = str_replace(" ", "-", $this->string);\r\n\r\n        return $this;\r\n    }\r\n\r\n    public function get()\r\n    {\r\n        return $this->string;\r\n    }\r\n\r\n}\r\n\r\n```\r\n\r\nAnd the implementation:\r\n\r\n```php\r\n<?php\r\nuse Foo;\r\n\r\n$string = new Foo(''test With A Lot Of Spaces'');\r\n\r\necho $string->replaceSpaces()->toUpper()->get();\r\n\r\n```\r\n\r\nWhich will return:\r\n```\r\n> TEST-WITH-A-LOT-OF-SPACES\r\n```\r\n\r\n**Bonus Example: Javascript**\r\n\r\n```javascript\r\nvar ShaneLessard = function() {\r\n    this.name = ''Shane'';\r\n    this.hair = ''not a whole lot of'';\r\n};\r\n\r\nShaneLessard.prototype.setName = function(name){\r\n    this.name = name;\r\n    return this;\r\n};\r\n\r\nShaneLessard.prototype.setHair = function(hair){\r\n    this.hair = hair;\r\n    return this;\r\n};\r\n\r\nShaneLessard.prototype.describe = function(){\r\n  console.log(this.name + " has " + this.hair + " hair.");\r\n};\r\n\r\nnew ShaneLessard()\r\n    .setName(''Young Shane'')\r\n    .setHair("so much")\r\n    .describe();\r\n```\r\nAnd you''ll wind up with the predictable output of:\r\n\r\n```> Young Shane has so much hair.```\r\n\r\nOther languages that allow method chaining include C++, Java, Python, Ruby and C#.', '2016-01-10 00:39:02', '2016-01-12 09:46:02');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `role` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'user'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `role`) VALUES
(1, 'Shane Lessard', 'lessardshane@gmail.com', '$2y$10$HzRTS7fLKE41/HsDnsC.QurUAcrQdYbMJ6SDwChsHbdvm8rCo4BCW', 'VRpSycURQ9pINXI64QKKPRneDBp3vbOR2Ks2K3xk7f5s85B15u0KJg9Nj9Aw', '2016-01-09 09:12:55', '2016-01-12 09:47:38', 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `posts_user_id_foreign` (`user_id`),
  ADD KEY `posts_title_index` (`title`),
  ADD KEY `posts_created_at_index` (`created_at`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
