<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Portfolio;
use Input;
use App\Http\Controllers\Auth;
use App\User;

use Illuminate\Http\Request;

class PortfolioController extends Controller {

    /**
     * PortfolioController constructor.
     */
    public function __construct()
    {
        //Check for admin status for all routes except index and show
        $this->middleware('admin', [
            'except' => [
                'index',
                'show',
            ]
        ]);
    }

    /**
	 * Display a listing of portfolio pieces.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data['portfolios'] = Portfolio::orderBy('weight', 'desc')->paginate(3);
        $data['title'] = "Recent Work";
		$data['admin'] = User::isAdmin();

        return view('portfolio.index', $data);
    }

	/**
	 * Show the form for creating a new portfolio piece.
	 *
	 * @return Response
	 */
	public function create()
	{
        $data['title'] = "New Portfolio Piece";

        return view('portfolio.create', $data);
	}

	/**
	 * Store a newly created portfolio piece in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        Portfolio::create(Input::all());

        return redirect('/portfolio');
    }

	/**
	 * Show the form for editing the specified portfolio piece.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $data['portfolio'] = Portfolio::find($id);
        $data['title'] = 'Edit ' . $data['portfolio']->title;

        return view('portfolio.edit', $data);
	}

	/**
	 * Update the specified portfolio piece in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        Portfolio::find($id)->update(Input::all());

        return redirect('/portfolio');
	}

	/**
	 * Remove the specified portfolio piece from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Portfolio::find($id)->delete();

        return redirect('/portfolio');
    }

}
