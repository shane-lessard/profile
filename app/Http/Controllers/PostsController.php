<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App;
use Input;
use App\Post;
use Parsedown;
use App\User;
use Illuminate\Http\Request;

class PostsController extends Controller {

    public function __construct()
    {
        $this->middleware('admin', [
            'except' => [
                'index',
                'show',
            ]
        ]);
    }

	/**
	 * Display a listing of the post.
	 *
	 * @return Response
	 */
	public function index()
	{

        $data = [
            'posts' => Post::orderBy('created_at', 'desc')->paginate(5),
            'title' => "All Articles",
        ];

        return view('posts.list', $data);

    }

	/**
	 * Show the form for creating a new post.
	 *
	 * @return Response
	 */
	public function create()
	{
         return view('posts.new', ['title' => 'New Post']);
	}

	/**
	 * Store a newly created post in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
            $inputs = Input::all();

            $inputs['user_id'] = 1;

            $post = Post::create($inputs);

            $post->save();

            return redirect('/articles');

	}

	/**
	 * Display the specified post.
	 *
	 * @param  string  $title
	 * @return Response
	 */
	public function show($title)
	{
        $url = rawurldecode(str_replace('-', ' ',$title));

        $post = Post::where('title', $url)->first();

        $parse = new Parsedown;

        $post->body = $parse->text($post->body);

        $data = [
            'post' => $post,
            'title' => $post->title,
        ];

		return view('posts.show', $data);
	}

	/**
	 * Show the form for editing the specified post.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $post = Post::find($id);

        return view('posts.update', [
            'post' => $post,
            'title' => "Edit Post: " . $post->title
        ]);

	}

	/**
	 * Update the specified post in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $inputs = Input::all();

        $post = Post::find($id);

        $post->update($inputs);

        return redirect('/articles/' . rawurlencode(str_replace(' ', '-', $post->title)));

	}

	/**
	 * Remove the specified post from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        Post::find($id)->delete();

        return redirect('/articles');

	}

}
